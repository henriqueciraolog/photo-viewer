package br.com.hciraolo.photosviewer.api

import br.com.hciraolo.photosviewer.model.Photo
import retrofit2.http.GET
import retrofit2.Response
import retrofit2.http.Query

interface API {
    @GET("photos")
    suspend fun fetchPhotos(
        @Query("page") page: Int,
        @Query("client_id") clientId: String = "52ed5e63ad1915fed2bbfd2326aade6b8549b050fc8367a7c105567476df2a81"
    ): Response<List<Photo>>
}