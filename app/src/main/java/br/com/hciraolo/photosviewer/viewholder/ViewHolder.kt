package br.com.hciraolo.photosviewer.viewholder

import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.photosviewer.R
import br.com.hciraolo.photosviewer.adapter.PhotoRecyclerViewAdapter
import br.com.hciraolo.photosviewer.databinding.ItemPhotoBinding
import br.com.hciraolo.photosviewer.model.Photo
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

class ViewHolder internal constructor(private val binding: ItemPhotoBinding) :
    RecyclerView.ViewHolder(
        binding.root
    ) {

    fun bind(photo: Photo, listerner: PhotoRecyclerViewAdapter.OnItemClickListerner) {
        val mContext = binding.root.context
        binding.vwPhoto.setOnClickListener { listerner.onItemClick(binding.imgPhoto, photo) }
        Picasso.with(mContext).load(photo.urls.small).networkPolicy(NetworkPolicy.OFFLINE).into(
            binding.imgPhoto, object : Callback {
                override fun onSuccess() {
                    //Sucesso!!
                }

                override fun onError() {
                    Picasso.with(mContext).load(photo.urls.small)
                        .error(R.drawable.ic_error_outline_black_100dp).into(
                            binding.imgPhoto
                        )
                }
            })
        binding.imgPhoto.contentDescription = photo.altDescription
    }
}