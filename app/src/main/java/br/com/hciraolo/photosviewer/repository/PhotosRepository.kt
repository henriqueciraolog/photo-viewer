package br.com.hciraolo.photosviewer.repository

import br.com.hciraolo.photosviewer.api.API
import br.com.hciraolo.photosviewer.api.ResultApi
import br.com.hciraolo.photosviewer.model.Photo

class PhotosRepository(private val api: API) {
    suspend fun getPhotos(page: Int = 1): ResultApi<List<Photo>?> {
        return try {
            val resp = api.fetchPhotos(page)
            if (resp.isSuccessful) {
                ResultApi.createResultApi(resp.body())
            } else {
                ResultApi.createResultApi(resp.errorBody())
            }
        } catch (e: Exception) {
            ResultApi.createResultApi(e)
        }
    }
}