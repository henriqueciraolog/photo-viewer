package br.com.hciraolo.photosviewer.api

import okhttp3.ResponseBody

class ResultApi<Success> {
    private constructor(value: Success) {
        this.value = value
    }

    private constructor(httpError: ResponseBody?) {
        this.httpError = httpError
    }

    private constructor(error: Throwable) {
        this.error = error
    }

    var value: Success? = null
        private set

    var httpError: ResponseBody? = null
        private set

    var error: Throwable? = null
        private set

    fun isSuccess() = value != null
    fun isHttpError() = httpError != null
    fun isError() = error != null


    companion object {
        fun <T> createResultApi(value: T) = ResultApi(value)
        fun <T> createResultApi(httpError: ResponseBody?) = ResultApi<T>(httpError)
        fun <T> createResultApi(error: Throwable) = ResultApi<T>(error)

    }
}