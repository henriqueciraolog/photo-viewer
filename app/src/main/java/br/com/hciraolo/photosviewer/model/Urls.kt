package br.com.hciraolo.photosviewer.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Urls(
    @SerializedName("small")
    val small: String
): Parcelable