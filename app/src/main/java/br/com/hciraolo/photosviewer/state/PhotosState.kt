package br.com.hciraolo.photosviewer.state

import br.com.hciraolo.photosviewer.model.Photo

sealed class PhotosEvent {
    data class AddNewPhotos(val list: List<Photo>): PhotosEvent()
}

sealed class PhotosState {
    object ShowLoading: PhotosState()
    object HideLoading: PhotosState()
}

sealed class PhotosInteractor {
    object GetPhotosReload: PhotosInteractor()
    object GetPhotos: PhotosInteractor()
}