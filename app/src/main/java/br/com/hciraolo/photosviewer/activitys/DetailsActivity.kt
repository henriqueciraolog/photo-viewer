package br.com.hciraolo.photosviewer.activitys

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import br.com.hciraolo.photosviewer.R
import br.com.hciraolo.photosviewer.model.Photo
import br.com.hciraolo.photosviewer.databinding.ActivityDetailsBinding
import com.squareup.picasso.Picasso

class DetailsActivity : AppCompatActivity() {
    private val photo: Photo by lazy { intent.getParcelableExtra(PHOTO) }
    private var binding: ActivityDetailsBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding?.root)
        setupLayout()
    }

    private fun setupLayout() {
        Picasso.with(this)
            .load(photo.urls.small)
            .error(R.drawable.ic_error_outline_black_100dp)
            .into(binding!!.imgPhotoFull)
    }

    companion object {
        const val PHOTO = "photo"
    }
}