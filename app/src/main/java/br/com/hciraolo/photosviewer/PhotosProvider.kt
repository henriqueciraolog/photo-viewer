package br.com.hciraolo.photosviewer

import br.com.hciraolo.photosviewer.api.API
import br.com.hciraolo.photosviewer.repository.PhotosRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private object HOLDER {
    val api: API = Retrofit.Builder()
        .baseUrl("https://api.unsplash.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(API::class.java)
    val REPOSITORY_INSTANCE: PhotosRepository = PhotosRepository(api)
}

private val photosRepository by lazy { HOLDER.REPOSITORY_INSTANCE }

fun providePhotosRepository() = photosRepository