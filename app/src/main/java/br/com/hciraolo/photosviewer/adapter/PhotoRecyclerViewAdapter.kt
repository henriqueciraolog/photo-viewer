package br.com.hciraolo.photosviewer.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.photosviewer.model.Photo
import br.com.hciraolo.photosviewer.databinding.ItemPhotoBinding
import br.com.hciraolo.photosviewer.viewholder.ViewHolder

class PhotoRecyclerViewAdapter(private val mListerner: OnItemClickListerner) :
    RecyclerView.Adapter<ViewHolder>() {
    private var mDataset: ArrayList<Photo> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val shot = mDataset[position]
        holder.bind(shot, mListerner)
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    fun updateList(newlist: List<Photo>?) {
        mDataset = ArrayList()
        mDataset.addAll(newlist!!)
        notifyDataSetChanged()
    }

    fun addList(addlist: List<Photo>?) {
        val pos = mDataset.size
        mDataset.addAll(addlist!!)
        notifyItemInserted(pos)
    }

    interface OnItemClickListerner {
        fun onItemClick(imageView: ImageView?, photo: Photo?)
    }
}