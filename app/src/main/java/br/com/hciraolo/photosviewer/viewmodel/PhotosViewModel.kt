package br.com.hciraolo.photosviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.hciraolo.photosviewer.api.ResultApi
import br.com.hciraolo.photosviewer.model.Photo
import br.com.hciraolo.photosviewer.providePhotosRepository
import br.com.hciraolo.photosviewer.repository.PhotosRepository
import br.com.hciraolo.photosviewer.state.PhotosEvent
import br.com.hciraolo.photosviewer.state.PhotosInteractor
import br.com.hciraolo.photosviewer.state.PhotosState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class PhotosViewModel : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main + SupervisorJob()

    private val event = MutableLiveData<PhotosEvent>()
    val viewEvent: LiveData<PhotosEvent>
        get() = event

    private val state = MutableLiveData<PhotosState>()
    val viewState: LiveData<PhotosState>
        get() = state

    private val repository: PhotosRepository = providePhotosRepository()

    private var page = 1

    private var endOfPage = false

    fun interpret(interactor: PhotosInteractor) {
        when (interactor) {
            is PhotosInteractor.GetPhotosReload -> {
                page = 1
                getPhotos()
            }
            is PhotosInteractor.GetPhotos -> {
                getPhotos()
            }
        }
    }

    private fun getPhotos() {
        if (!endOfPage) {
            state.postValue(PhotosState.ShowLoading)
            launch {
                val result = repository.getPhotos(page)
                afterGetPhotos(result)
            }
        }
    }

    private fun afterGetPhotos(result: ResultApi<List<Photo>?>) {
        state.postValue(PhotosState.HideLoading)
        when {
            result.isSuccess() -> {
                result.value?.let { it ->
                    if (it.isNotEmpty()) {
                        page++
                        event.postValue(result.value?.let { PhotosEvent.AddNewPhotos(it) })
                    } else {
                        endOfPage = true
                    }
                }
            }
            result.isHttpError() -> {

            }
            result.isError() -> {

            }
        }
    }
}