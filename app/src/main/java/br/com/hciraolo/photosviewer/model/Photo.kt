package br.com.hciraolo.photosviewer.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo (
    @SerializedName("alt_description")
    val altDescription: String?,
    @SerializedName("urls")
    val urls: Urls
): Parcelable