package br.com.hciraolo.photosviewer.activitys

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Surface
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import br.com.hciraolo.photosviewer.viewmodel.PhotosViewModel
import br.com.hciraolo.photosviewer.R
import br.com.hciraolo.photosviewer.adapter.PhotoRecyclerViewAdapter
import br.com.hciraolo.photosviewer.adapter.PhotoRecyclerViewAdapter.OnItemClickListerner
import br.com.hciraolo.photosviewer.model.Photo
import br.com.hciraolo.photosviewer.databinding.ActivityMainBinding
import br.com.hciraolo.photosviewer.listerner.EndlessRecyclerViewScrollListener
import br.com.hciraolo.photosviewer.state.PhotosEvent
import br.com.hciraolo.photosviewer.state.PhotosInteractor
import br.com.hciraolo.photosviewer.state.PhotosState

class MainActivity : AppCompatActivity(), OnRefreshListener, OnItemClickListerner {
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(
            LayoutInflater.from(
                this
            )
        )
    }

    private var rcvAdapter: PhotoRecyclerViewAdapter = PhotoRecyclerViewAdapter(this)

    private val display by lazy { (getSystemService(WINDOW_SERVICE) as WindowManager).defaultDisplay }

    private val qtdeColunas by lazy { if (display.rotation == Surface.ROTATION_0 || display.rotation == Surface.ROTATION_180) 2 else 4 }

    private val rcvLayoutManager by lazy {
        StaggeredGridLayoutManager(
            qtdeColunas,
            StaggeredGridLayoutManager.VERTICAL
        )
    }

    private val scrollListener by lazy {
        object :
            EndlessRecyclerViewScrollListener(rcvLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                viewModel.interpret(PhotosInteractor.GetPhotos)
            }
        }
    }

    private val viewModel by lazy {
        defaultViewModelProviderFactory.create(
            PhotosViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupLayout()


        viewModel.viewEvent.observe(this, {
            when (it) {
                is PhotosEvent.AddNewPhotos -> {
                    addPhotosToList(it.list)
                }
            }
        })

        viewModel.viewState.observe(this, {
            when (it) {
                PhotosState.ShowLoading -> {
                    binding.srlRefresh.isRefreshing = true
                }
                PhotosState.HideLoading -> {
                    binding.srlRefresh.isRefreshing = false
                }
            }
        })

        viewModel.interpret(PhotosInteractor.GetPhotosReload)
    }

    private fun addPhotosToList(list: List<Photo>) {
        rcvAdapter.addList(list)
    }

    private fun setupLayout() {
        supportActionBar?.setTitle(R.string.app_name)
        binding.srlRefresh.setOnRefreshListener(this)
        binding.rcvPhotos.setHasFixedSize(true)

        binding.rcvPhotos.layoutManager = rcvLayoutManager
        binding.rcvPhotos.adapter = rcvAdapter
        binding.rcvPhotos.addOnScrollListener(scrollListener)
    }

    override fun onRefresh() {
        rcvAdapter.updateList(ArrayList())
        viewModel.interpret(PhotosInteractor.GetPhotosReload)
    }

    override fun onItemClick(imageView: ImageView?, photo: Photo?) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.PHOTO, photo)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            Pair(imageView, getString(R.string.transition_name_shot))
        )
        ActivityCompat.startActivity(this, intent, options.toBundle())
    }
}