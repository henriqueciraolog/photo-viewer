package br.com.hciraolo.photosviewer.viewmodel

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.hciraolo.photosviewer.api.ResultApi
import br.com.hciraolo.photosviewer.providePhotosRepository
import br.com.hciraolo.photosviewer.repository.PhotosRepository
import br.com.hciraolo.photosviewer.state.PhotosEvent
import br.com.hciraolo.photosviewer.state.PhotosInteractor
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PhotosViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    lateinit var repository: PhotosRepository

    @Before
    fun setUp() {
        repository = mockk()
        mockkStatic("br.com.hciraolo.photosviewer.PhotosProviderKt")
        every { providePhotosRepository() } returns repository
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `when interpret calls with GetPhotos, calls repository getPhotos`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(listOf(mockk()))

        val sut = PhotosViewModel()

        sut.interpret(PhotosInteractor.GetPhotos)

        coVerify { repository.getPhotos(any()) }
    }

    @Test
    fun `when interpret calls with GetPhotosReload, calls repository getPhotos`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(listOf(mockk()))

        val sut = PhotosViewModel()

        sut.interpret(PhotosInteractor.GetPhotosReload)

        coVerify { repository.getPhotos(any()) }
    }

    @Test
    fun `when interpret calls with GetPhotos, but repository returns without photos, nothing is send`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(emptyList())

        val sut = PhotosViewModel()

        sut.interpret(PhotosInteractor.GetPhotos)

        assert(sut.viewEvent.value == null)
    }

    @Test
    fun `when interpret calls with GetPhotos, but repository returns with photos, send AddNewPhotos`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(listOf(mockk()))

        val sut = PhotosViewModel()

        sut.viewEvent.observeForever {
            assert(it is PhotosEvent.AddNewPhotos)
        }

        sut.interpret(PhotosInteractor.GetPhotos)
    }

    @Test
    fun `when interpret calls with GetPhotosReload, but repository returns without photos, nothing is send`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(emptyList())

        val sut = PhotosViewModel()

        sut.interpret(PhotosInteractor.GetPhotosReload)

        assert(sut.viewEvent.value == null)
    }

    @Test
    fun `when interpret calls with GetPhotosReload, but repository returns with photos, send AddNewPhotos`() = runBlockingTest {
        coEvery { repository.getPhotos(any()) } returns ResultApi.Companion.createResultApi(listOf(mockk()))

        val sut = PhotosViewModel()

        sut.viewEvent.observeForever {
            assert(it is PhotosEvent.AddNewPhotos)
        }

        sut.interpret(PhotosInteractor.GetPhotosReload)
    }

}