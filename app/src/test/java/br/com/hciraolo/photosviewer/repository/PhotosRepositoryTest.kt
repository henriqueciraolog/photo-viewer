package br.com.hciraolo.photosviewer.repository

import br.com.hciraolo.photosviewer.api.API
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Response
import java.lang.IllegalArgumentException

@RunWith(JUnit4::class)
class PhotosRepositoryTest {

    @Test
    fun `when getPhotos call api fetchPhotos`() {
        val api = mockk<API>()

        coEvery { api.fetchPhotos(any()) } returns Response.success(ArrayList())

        val repository = PhotosRepository(api)

        runBlocking {
            repository.getPhotos(1)
        }

        coVerify { api.fetchPhotos(any()) }

    }

    @Test
    fun `when getPhotos is get success, return response with success`() {
        val api = mockk<API>()

        coEvery { api.fetchPhotos(any()) } returns Response.success(ArrayList())

        val repository = PhotosRepository(api)

        runBlocking {
            val result = repository.getPhotos(1)

            assert(result.isSuccess())
        }

    }

    @Test
    fun `when getPhotos is get error, return response with httpError`() {
        val api = mockk<API>()

        coEvery { api.fetchPhotos(any()) } returns Response.error(500, ResponseBody.create(null, "{}"))

        val repository = PhotosRepository(api)

        runBlocking {
            val result = repository.getPhotos(1)

            assert(result.isHttpError())
        }

    }

    @Test
    fun `when getPhotos cause error, return response with error`() {
        val api = mockk<API>()

        coEvery { api.fetchPhotos(any()) } throws IllegalArgumentException()

        val repository = PhotosRepository(api)

        runBlocking {
            val result = repository.getPhotos(1)

            assert(result.isError())
        }

    }
}